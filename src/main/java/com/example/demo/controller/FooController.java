package com.example.demo.controller;

import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.Foo;
import com.example.demo.service.IFooService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/foo")
public class FooController {

    @Autowired
    private IFooService fooService;

    @GetMapping()
    public Flux<Foo> getAllfoo() {
        return fooService.getAll();
    }

    @GetMapping("{id}")
    public Mono<ResponseEntity<Foo>> getFooById(@PathVariable(value = "id") int fooId) {
        return fooService.findById(fooId)
                .map(savedFoo -> ResponseEntity.ok(savedFoo))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("{id}")
    public Mono<ResponseEntity<Void>> deleteFoo(@PathVariable(value = "id") int fooId) {
        fooService.delete(fooId)
                .map(savedFoo -> ResponseEntity.ok(HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        return null;
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity handleFooNotFoundException(ResourceNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }
}
