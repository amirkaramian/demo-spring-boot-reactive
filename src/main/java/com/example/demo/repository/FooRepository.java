package com.example.demo.repository;

import com.example.demo.model.Foo;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;

@Repository
public class FooRepository {
    private static final HashMap<Integer, Foo> FOO_MAP;

    static {
        FOO_MAP = new HashMap<>();
        FOO_MAP.put(1, new Foo(1, "Foo item 1"));
        FOO_MAP.put(2, new Foo(2, "Foo item 2"));
        FOO_MAP.put(3, new Foo(3, "Foo item 3"));
        FOO_MAP.put(4, new Foo(4, "Foo item 4"));
        FOO_MAP.put(5, new Foo(5, "Foo item 5"));
        FOO_MAP.put(6, new Foo(6, "Foo item 6"));
        FOO_MAP.put(7, new Foo(7, "Foo item 7"));
        FOO_MAP.put(8, new Foo(8, "Foo item 8"));
        FOO_MAP.put(9, new Foo(9, "Foo item 9"));
        FOO_MAP.put(10, new Foo(10, "Foo item 10"));
    }

    public Mono<Foo> geFooById(int id) {

        return Mono.justOrEmpty(FOO_MAP.get(id));
    }

    public Flux<Foo> getAllFoo() {
        return Flux.fromIterable(FOO_MAP.values());
    }

    public Mono<Boolean> deleteFoo(Foo foo) {
        return Mono.just(FOO_MAP.remove(foo.getId(), FOO_MAP.get(foo.getId())));
    }
}
