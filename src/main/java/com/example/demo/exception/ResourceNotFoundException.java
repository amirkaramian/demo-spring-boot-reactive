package com.example.demo.exception;

public class ResourceNotFoundException extends RuntimeException{

    public ResourceNotFoundException(int fooId)
    {
        super("Foo not found " + fooId);
    }
}
