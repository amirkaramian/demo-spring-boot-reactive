package com.example.demo;

import com.example.demo.model.Foo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Random;

@Component
public class FooCronComponentService {
    private static final Logger LOGGER = LoggerFactory.getLogger(FooCronComponentService.class);

    WebClient client = WebClient.create("http://localhost:8080");
    private static final Random random = new Random(10);

    @Scheduled(fixedRate = 1000)
    public void consume() {

        int id = random.nextInt(1, 10);
        Mono<Foo> fooMono = client.get()
                .uri("/api/v1/foo/{id}", id)
                .retrieve()
                .bodyToMono(Foo.class);

        fooMono.subscribe(foo -> LOGGER.info("Foo: {}", foo));


    }
}
