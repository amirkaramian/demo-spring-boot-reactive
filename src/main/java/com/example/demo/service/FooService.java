package com.example.demo.service;


import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.Foo;
import com.example.demo.repository.FooRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class FooService implements IFooService {


    @Autowired
    private FooRepository fooRepository;


    @Override
    public Flux<Foo> getAll() {
        return fooRepository.getAllFoo();
    }

    @Override
    public Mono<Foo> findById(int fooId) {
        return fooRepository.geFooById(fooId);
    }

    @Override
    public Mono<Boolean> delete(int fooId) {
        Mono<Foo> foo = fooRepository.geFooById(fooId);
        if (foo.block() == null)
            throw new ResourceNotFoundException(fooId);
        return fooRepository.deleteFoo(foo.block());
    }


}
