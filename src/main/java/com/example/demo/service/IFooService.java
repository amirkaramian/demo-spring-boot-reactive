package com.example.demo.service;

import com.example.demo.model.Foo;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.netty.ChannelPipelineConfigurer;

public interface IFooService {
    Flux<Foo> getAll();

    Mono<Foo> findById(int fooId);

    Mono<Boolean> delete(int fooId);
}
